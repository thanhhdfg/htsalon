package com.salon.ht.entity.payload;

public class ServiceSearchRes {
    private Long bookingId;

    private Long id;

    private String name;

    private Integer type;

    private Long price;

    private Long duration;

}
